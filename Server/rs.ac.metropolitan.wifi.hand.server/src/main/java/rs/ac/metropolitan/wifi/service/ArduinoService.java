package rs.ac.metropolitan.wifi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.metropolitan.wifi.config.Config;
import rs.ac.metropolitan.wifi.db.ArduinoDao;
import rs.ac.metropolitan.wifi.model.Position;

@Service
public class ArduinoService {

    @Autowired
    ArduinoDao dao;

    @Autowired
    Config config;

    public void storeData(Position position) {
        dao.storeData(position);
    }

    public Position getLatestData() {
        return dao.getLatestData();
    }

    public void resetPosition() {
        Position resetPosition = new Position(
                config.getStarterPosP1(),
                config.getStarterPosP2(),
                config.getStarterPosP3(),
                config.getStarterPosP4(),
                config.getStarterPosP5(),
                config.getStarterPosP6());
        dao.storeData(resetPosition);
    }
}
