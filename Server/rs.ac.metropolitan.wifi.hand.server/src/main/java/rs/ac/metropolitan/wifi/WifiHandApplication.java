package rs.ac.metropolitan.wifi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WifiHandApplication {

    public static void main(String[] args) {
        SpringApplication.run(WifiHandApplication.class, args);
    }

}
