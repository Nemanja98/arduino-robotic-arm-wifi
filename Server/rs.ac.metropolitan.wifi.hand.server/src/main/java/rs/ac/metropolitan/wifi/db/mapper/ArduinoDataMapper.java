package rs.ac.metropolitan.wifi.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import rs.ac.metropolitan.wifi.model.Position;

public class ArduinoDataMapper implements RowMapper<Position> {

    @Override
    public Position mapRow(ResultSet rs, int i) throws SQLException {
        Position data = new Position();

        data.setP1(rs.getInt("P1"));
        data.setP2(rs.getInt("P2"));
        data.setP3(rs.getInt("P3"));
        data.setP4(rs.getInt("P4"));
        data.setP5(rs.getInt("P5"));
        data.setP6(rs.getInt("P6"));

        return data;

    }

}