package rs.ac.metropolitan.wifi.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
public class Config {

    @Value("${wifi.hand.starter.position.p1}")
    public int starterPosP1;

    @Value("${wifi.hand.starter.position.p2}")
    public int starterPosP2;

    @Value("${wifi.hand.starter.position.p3}")
    public int starterPosP3;

    @Value("${wifi.hand.starter.position.p4}")
    public int starterPosP4;

    @Value("${wifi.hand.starter.position.p5}")
    public int starterPosP5;

    @Value("${wifi.hand.starter.position.p6}")
    public int starterPosP6;

}
