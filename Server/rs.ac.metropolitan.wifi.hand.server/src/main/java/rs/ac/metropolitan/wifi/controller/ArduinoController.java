package rs.ac.metropolitan.wifi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import rs.ac.metropolitan.wifi.model.Position;
import rs.ac.metropolitan.wifi.service.ArduinoService;

import org.springframework.ui.Model;
import org.springframework.web.client.RestTemplate;

@SessionAttributes({"position"})
@Controller
public class ArduinoController {

    @Autowired
    ArduinoService arduinoService;

    @RequestMapping("/")
    public String home(Model theModel) {
        Position position = arduinoService.getLatestData();
        theModel.addAttribute("position", position);
        return "index.html";
    }

    @GetMapping(value = "/api/v1/position")
    public ResponseEntity<String> getPos() {

        Position response = arduinoService.getLatestData();
        return new ResponseEntity<>(response.toString(), HttpStatus.OK);
    }

    @PostMapping(value = "/api/v1/record")
    public ResponseEntity<String> recordPosition(@RequestBody Position position) {

        arduinoService.storeData(position);
        return new ResponseEntity<>("Data recorded!", HttpStatus.OK);
    }

    @PutMapping("/api/v1/reset")
    public ResponseEntity<String> resetPosition() {

        arduinoService.resetPosition();
        return new ResponseEntity<>("Position reset!", HttpStatus.OK);
    }

    @PostMapping("/ui/v1/record")
    public String addPosition(@ModelAttribute("position") Position position) {

        final String uri = "http://localhost:8080/api/v1/record";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(uri, position, String.class);
        return "redirect:/";
    }

}
