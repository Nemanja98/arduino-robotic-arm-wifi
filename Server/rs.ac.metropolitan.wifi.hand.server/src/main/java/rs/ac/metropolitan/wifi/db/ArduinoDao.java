package rs.ac.metropolitan.wifi.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import rs.ac.metropolitan.wifi.db.mapper.ArduinoDataMapper;
import rs.ac.metropolitan.wifi.model.Position;

@Component
public class ArduinoDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public String state = "default";

    private static final String CREATE_TABLE = "CREATE TABLE \"POSITION\" (\n" +
            "	\"ID\"	INTEGER,\n" +
            "	\"P1\"	INTEGER,\n" +
            "	\"P2\"	INTEGER,\n" +
            "	\"P3\"	INTEGER,\n" +
            "	\"P4\"	INTEGER,\n" +
            "	\"P5\"	INTEGER,\n" +
            "	\"P6\"	INTEGER,\n" +
            "	PRIMARY KEY(\"ID\" AUTOINCREMENT)\n" +
            ")";

    private static final String INSERT_DATA = "INSERT INTO POSITION (P1,P2,P3,P4,P5,P6)"
            + " VALUES (?,?,?,?,?,?)";
    private static final String SELECT_LATEST_POSITION = "SELECT * FROM POSITION ORDER BY ID DESC LIMIT 1";


    public void storeData(Position position) {

        jdbcTemplate.execute(INSERT_DATA, new PreparedStatementCallback<Boolean>() {

            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {

                ps.setInt(1, position.getP1());
                ps.setInt(2, position.getP2());
                ps.setInt(3, position.getP3());
                ps.setInt(4, position.getP4());
                ps.setInt(5, position.getP5());
                ps.setInt(6, position.getP6());
                return ps.execute();
            }
        });
    }

    public Position getLatestData() {
        String sql = SELECT_LATEST_POSITION;
        List<Position> data = jdbcTemplate.query(sql, new ArduinoDataMapper());
        return data.get(0);
    }
}
