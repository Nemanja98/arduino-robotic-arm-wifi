package rs.ac.metropolitan.wifi.model;

import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.Max;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class Position {

    @Min(value = 0, message = "Tickets must be greater than or equal to 0 ")
    @Max(value = 180, message = "Tickets must be smaller than or equal to 180")
    private int p1;

    @Min(value = 15, message = "Tickets must be greater than or equal to 15")
    @Max(value = 165, message = "Tickets must be smaller than or equal to 165")
    private int p2;

    @Min(value = 0, message = "Tickets must be greater than or equal to 0")
    @Max(value = 180, message = "Tickets must be smaller than or equal to 180")
    private int p3;

    @Min(value = 0, message = "Tickets must be greater than or equal to 0")
    @Max(value = 180, message = "Tickets must be smaller than or equal to 180")
    private int p4;

    @Min(value = 0, message = "Tickets must be greater than or equal to 0")
    @Max(value = 180, message = "Tickets must be smaller than or equal to 180")
    private int p5;

    @Min(value = 10, message = "Tickets must be greater than or equal to 10")
    @Max(value = 73, message = "Tickets must be smaller than or equal to 73")
    private int p6;

    public String toString() {
        return p1 + "," + p2 + "," + p3 + "," + p4 + "," + p5 + "," + p6 + ";";
    }

}
