function pos1() {
    var position = new Object();
    position.p1 = 0;
    position.p2 = 45;
    position.p3 = 180;
    position.p4 = 180;
    position.p5 = 90;
    position.p6 = 10;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/api/v1/record", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(position));
    location.reload();
    return false;
}

function pos2() {
    var position = new Object();
    position.p1 = 0;
    position.p2 = 90;
    position.p3 = 180;
    position.p4 = 180;
    position.p5 = 90;
    position.p6 = 10;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/api/v1/record", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(position));
    location.reload();
    return false;
}

function pos3() {
    var position = new Object();
    position.p1 = 0;
    position.p2 = 90;
    position.p3 = 180;
    position.p4 = 180;
    position.p5 = 90;
    position.p6 = 60;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/api/v1/record", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(position));
    location.reload();
    return false;
}

function pos4() {
    var position = new Object();
    position.p1 = 0;
    position.p2 = 45;
    position.p3 = 180;
    position.p4 = 45;
    position.p5 = 0;
    position.p6 = 60;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/api/v1/record", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(position));
    location.reload();
    return false;
}

function pos5() {
    var position = new Object();
    position.p1 = 180;
    position.p2 = 45;
    position.p3 = 180;
    position.p4 = 45;
    position.p5 = 0;
    position.p6 = 60;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/api/v1/record", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(position));
    location.reload();
    return false;
}

function pos6() {
    var position = new Object();
    position.p1 = 0;
    position.p2 = 90;
    position.p3 = 180;
    position.p4 = 180;
    position.p5 = 90;
    position.p6 = 60;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/api/v1/record", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(position));
    location.reload();
    return false;
}

function pos7() {
    var position = new Object();
    position.p1 = 0;
    position.p2 = 90;
    position.p3 = 180;
    position.p4 = 180;
    position.p5 = 90;
    position.p6 = 10;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/api/v1/record", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(position));
    location.reload();
    return false;
}

function pos8() {
    var position = new Object();
    position.p1 = 0;
    position.p2 = 100;
    position.p3 = 100;
    position.p4 = 100;
    position.p5 = 100;
    position.p6 = 73;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/api/v1/record", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(position));
    location.reload();
    return false;
}

function pos9() {
    var position = new Object();
    position.p1 = 39;
    position.p2 = 39;
    position.p3 = 39;
    position.p4 = 39;
    position.p5 = 39;
    position.p6 = 39;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/api/v1/record", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(position));
    location.reload();
    return false;
}

function reset() {
    var position = new Object();
    position.p1 = 90;
    position.p2 = 45;
    position.p3 = 180;
    position.p4 = 180;
    position.p5 = 90;
    position.p6 = 10;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/api/v1/record", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(position));
    location.reload();
    return false;
}