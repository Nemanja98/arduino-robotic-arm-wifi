#include <Braccio.h>
#include <Servo.h>

Servo base;
Servo shoulder;
Servo elbow;
Servo wrist_ver;
Servo wrist_rot;
Servo gripper;

void setup() {
  Serial.begin(115200);
  Braccio.begin();
}

int positions[5];

void loop() {
  if (Serial.available() > 0) {
    positions[0] =  Serial.readStringUntil(',').toInt(); // Range: 0-180
    positions[1] =  Serial.readStringUntil(',').toInt(); // Range: 15-165
    positions[2] =  Serial.readStringUntil(',').toInt(); // Range: 0-180
    positions[3] =  Serial.readStringUntil(',').toInt(); // Range: 0-180
    positions[4] =  Serial.readStringUntil(',').toInt(); // Range: 0-180
    positions[5] =  Serial.readStringUntil(';').toInt(); // Range: 10-73
    Braccio.ServoMovement(20, positions[0], positions[1], positions[2], positions[3], positions[4], positions[5]);
  }
}
