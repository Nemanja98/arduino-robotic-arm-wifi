#include "ESP8266WiFi.h"
#include "ESP8266HTTPClient.h"

const char* ssid = "zverkan"; //  Wi-Fi ssID
const char* password = ""; // Wi-Fi Password

void setup(void)
{

  Serial.begin(115200);
  // Connect to WiFi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
  }
}

void loop()
{
  if (WiFi.status() == WL_CONNECTED) { // Check Wi-Fi connection status
    HTTPClient http;    //Declare object of class HTTPClient
    http.begin("http://192.168.43.211:8080/api/v1/position"); // Specify request destination
    http.addHeader("Content-Type", "application/json");  // Specify content-type header
    int httpCode = http.GET();   // Send the request
    String payload = http.getString();
    Serial.print(payload);
    Serial.flush();
    http.end();  // Close connection
  } else {
    Serial.println("Error in WiFi connection");
  }
  delay(1000);  // Send a request every 1 second
}
